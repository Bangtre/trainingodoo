# -*- coding: utf-8 -*-
{
    'name': "Training Bootcamp Odoo",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Hasan",
    'website': "http://www.jukesolutions.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/15.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','stock','mail'],

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/sequence_data.xml',
        'views/views.xml',
        'views/templates.xml',
        'wizard/training_wizard_view.xml',
        'views/partner_views.xml',
        'views/menuitem.xml',
        'views/scheduler_data.xml',
        'report/report_action.xml',
        'report/report_training_session.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
